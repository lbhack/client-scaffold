export default {
  kyc_account_verification: 'Account verification',
  kyc_address_details: 'Address details',
  kyc_required_documents: 'Required documents',
  kyc_photo_verification: 'Photo verification',
  kyc_photo_explain: `To be sure it's you who submits the documents, please make a photo with a unique generated key`,
  kyc_show_key: 'Show the key',
  kyc_verification_key: 'Verification key',
  kyc_not_all_docs: 'Please upload all the documents before submit',
  kyc_upload_success: 'Information uploaded. Now please wait for approval',
  kyc_current_account_state: 'Current account state',
  kyc_approved_msg: 'Your account is approved. All actions that requires verification are now available',
  kyc_waiting_msg: `Your verification request is submitted. Please wait until we review the information.
                    You will be able to update details once the request is reviewed`,
  kyc_rejected_msg: 'Unfortunately, your account was rejected with reason: %{reason}',
  kyc_approved_title: 'Account approved'
}
