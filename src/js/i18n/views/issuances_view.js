export default {
  iss_submit_success: 'Tokens successfully issued',
  iss_no_balance: 'No account with such email or receiver has no %{asset} balance. If so, please ask him to add it first'
}
