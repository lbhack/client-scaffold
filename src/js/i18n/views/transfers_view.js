export default {
  tr_send: 'Send',
  tr_confirm: 'Payment confirmation',
  tr_recipient_not_found: 'Recipient not found',
  tr_pay_fee_for_rec: 'Pay fee for recipient',
  tr_successful: 'Payment successful',
  tr_no_assets: 'No transferable assets in your wallet',
  tr_no_assets_exp: 'This feature will be available once you have any transferable asset',
  tr_no_assets_exists: 'No assets exists in the system',
  tr_no_assets_exists_exp: 'This feature will be available once  any asset appears'
}
