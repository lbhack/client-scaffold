export default {
  withdraw_withdrawal: 'Withdrawal',
  withdraw_confirm: 'Withdrawal confirmation',
  withdraw_how_much: 'Minimal withdrawal amount is %{value} %{asset}',
  withdraw_minimal: 'Also note that network fee will be separately charged from amount of withdrawal transaction',
  withdraw_network_fee: 'Please note that network fee will be separately charged from amount of withdrawal transaction',
  withdraw_wallet: '%{asset} withdrawal address',
  withdraw_success: 'Withdrawal request successfully created',
  withdraw_balance: 'Your balance is %{balance} %{token}',
  withdraw_error_is_trying_to_send_to_yourself: 'Sender can\'t be a recipient! Make sure the address is correct',
  withdraw_error_invalid_address: 'Invalid wallet address',
  withdraw_error_insufficient_funds: 'Insufficient funds',
  withdraw_error_minimum_amount: 'The minimum amount is %{value} %{asset}',
  withdraw_no_assets: 'No withdrawable assets in your wallet',
  withdraw_no_assets_exp: 'This feature will be available once you have any withdrawable asset'
}
