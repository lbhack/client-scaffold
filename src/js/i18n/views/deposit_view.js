export default {
  dep_deposit: 'Deposit',
  dep_how_to: 'To make a deposit send BTC to the wallet address provided below',
  dep_how_long: 'Coins will be deposited immediately after 6 network confirmations',
  dep_send_only_asset: 'Send only %{asset} to this deposit address',
  dep_where_to: 'Send funds to this BTC address',
  dep_copied: 'Copied',
  dep_no_assets: 'No assets can be deposited',
  dep_no_assets_exp: 'This feature will be available once you have any asset that can be deposited'
}
