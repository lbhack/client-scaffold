export const accountResponse = {
  '_links': {
    'self': {
      'href': 'https://staging.app.sun.swarm.fund/accounts/GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN'
    },
    'transactions': {
      'href': 'https://staging.app.sun.swarm.fund/accounts/GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN/transactions{?cursor,limit,order}',
      'templated': true
    },
    'operations': {
      'href': 'https://staging.app.sun.swarm.fund/accounts/GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN/operations{?cursor,limit,order}',
      'templated': true
    },
    'payments': {
      'href': 'https://staging.app.sun.swarm.fund/accounts/GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN/payments{?cursor,limit,order}',
      'templated': true
    }
  },
  'id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
  'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
  'is_blocked': false,
  'block_reasons_i': 0,
  'block_reasons': [],
  'account_type_i': 2,
  'account_type': 'AccountTypeGeneral',
  'thresholds': {
    'low_threshold': 0,
    'med_threshold': 0,
    'high_threshold': 0
  },
  'balances': [
    {
      'balance_id': 'BDH6RGU2WHRDXAIDK5ALIW7NOTBOI6PU63Q7ZXYD6R5A6RPA5ELDSIHT',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'ETH',
      'balance': '84.009021',
      'locked': '0.681095',
      'require_review': false
    },
    {
      'balance_id': 'BDSCHZZB6ZBG2YIXBSD2PNFRIB7SQTB5X5X3N5ZY5RAJQBLCJ7FPG2UV',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'BTC',
      'balance': '96.258068',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BCJEWTZGGKV4JSIM65ZU5TIXYVYY5JVZL3EI33FCLYZLJNSAE6FLWLG2',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'SWM2',
      'balance': '0.163281',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BACSFUM3UI5QETD47II7HRZ3WIIYCDQEJT3L5SQ4ETWIXKLAIY27PWC7',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'SWM',
      'balance': '174.189583',
      'locked': '2.349585',
      'require_review': false
    },
    {
      'balance_id': 'BBCLRXJ4NCZSZGSVGKVEJT6FZDXYBYTSW5PRKVAVLORIV6TJB3MSQ7EX',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'SWM3',
      'balance': '36991.183131',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BBSW2CF2YPP2P5B3477Q7UEBTFTB6MUEJM2RIS57MPZZUH6ZTF3G55FI',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'SWM4',
      'balance': '1.241503',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BASKOU2DNHAYK46XRR77RJYZ2VZNXXBYDV3DKKQ72TTBSEBRYBZ2RHJ2',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'SPF',
      'balance': '0.000000',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BA7FHF5GMQXIQINWMBVC3RD6INQ26SGUH4B2ZSKIDRDBRJ7RAJ5PJYLE',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'Test',
      'balance': '148.622709',
      'locked': '0.000000',
      'require_review': false
    },
    {
      'balance_id': 'BAWFHIZBBWVZ5TUTCYVLS6VELMHRE6TKXYVAULWSLCD6MWYAPRDPH2XN',
      'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
      'asset': 'heyhey',
      'balance': '0.000000',
      'locked': '0.000000',
      'require_review': false
    }
  ],
  'signers': [
    {
      'public_key': 'GAMH2KJW65PKSH55EZRH5JESQA4LSNJMAS553MY4SWQ6XTEWZVS32NYY',
      'weight': 255,
      'signer_type_i': 2097151,
      'signer_types': [
        {
          'name': 'SignerTypeReader',
          'value': 1
        },
        {
          'name': 'SignerTypeNotVerifiedAccManager',
          'value': 2
        },
        {
          'name': 'SignerTypeGeneralAccManager',
          'value': 4
        },
        {
          'name': 'SignerTypeDirectDebitOperator',
          'value': 8
        },
        {
          'name': 'SignerTypeAssetManager',
          'value': 16
        },
        {
          'name': 'SignerTypeAssetRateManager',
          'value': 32
        },
        {
          'name': 'SignerTypeBalanceManager',
          'value': 64
        },
        {
          'name': 'SignerTypeIssuanceManager',
          'value': 128
        },
        {
          'name': 'SignerTypeInvoiceManager',
          'value': 256
        },
        {
          'name': 'SignerTypePaymentOperator',
          'value': 512
        },
        {
          'name': 'SignerTypeLimitsManager',
          'value': 1024
        },
        {
          'name': 'SignerTypeAccountManager',
          'value': 2048
        },
        {
          'name': 'SignerTypeCommissionBalanceManager',
          'value': 4096
        },
        {
          'name': 'SignerTypeOperationalBalanceManager',
          'value': 8192
        },
        {
          'name': 'SignerTypeEventsChecker',
          'value': 16384
        },
        {
          'name': 'SignerTypeExchangeAccManager',
          'value': 32768
        },
        {
          'name': 'SignerTypeSyndicateAccManager',
          'value': 65536
        },
        {
          'name': 'SignerTypeUserAssetManager',
          'value': 131072
        },
        {
          'name': 'SignerTypeUserIssuanceManager',
          'value': 262144
        },
        {
          'name': 'SignerTypeWithdrawManager',
          'value': 524288
        },
        {
          'name': 'SignerTypeFeesManager',
          'value': 1048576
        }
      ],
      'signer_identity': 0,
      'signer_name': ''
    },
    {
      'public_key': 'GCZ2KYVKZMOB3PA6OM7NHNRXYMBKRPXJHCUQQVXDGFDFOAF6ODQAHOYQ',
      'weight': 255,
      'signer_type_i': 4194303,
      'signer_types': [
        {
          'name': 'SignerTypeReader',
          'value': 1
        },
        {
          'name': 'SignerTypeNotVerifiedAccManager',
          'value': 2
        },
        {
          'name': 'SignerTypeGeneralAccManager',
          'value': 4
        },
        {
          'name': 'SignerTypeDirectDebitOperator',
          'value': 8
        },
        {
          'name': 'SignerTypeAssetManager',
          'value': 16
        },
        {
          'name': 'SignerTypeAssetRateManager',
          'value': 32
        },
        {
          'name': 'SignerTypeBalanceManager',
          'value': 64
        },
        {
          'name': 'SignerTypeIssuanceManager',
          'value': 128
        },
        {
          'name': 'SignerTypeInvoiceManager',
          'value': 256
        },
        {
          'name': 'SignerTypePaymentOperator',
          'value': 512
        },
        {
          'name': 'SignerTypeLimitsManager',
          'value': 1024
        },
        {
          'name': 'SignerTypeAccountManager',
          'value': 2048
        },
        {
          'name': 'SignerTypeCommissionBalanceManager',
          'value': 4096
        },
        {
          'name': 'SignerTypeOperationalBalanceManager',
          'value': 8192
        },
        {
          'name': 'SignerTypeEventsChecker',
          'value': 16384
        },
        {
          'name': 'SignerTypeExchangeAccManager',
          'value': 32768
        },
        {
          'name': 'SignerTypeSyndicateAccManager',
          'value': 65536
        },
        {
          'name': 'SignerTypeUserAssetManager',
          'value': 131072
        },
        {
          'name': 'SignerTypeUserIssuanceManager',
          'value': 262144
        },
        {
          'name': 'SignerTypeWithdrawManager',
          'value': 524288
        },
        {
          'name': 'SignerTypeFeesManager',
          'value': 1048576
        },
        {
          'name': 'SignerTypeTxSender',
          'value': 2097152
        }
      ],
      'signer_identity': 0,
      'signer_name': ''
    }
  ],
  'limits': {
    'daily_out': '9223372036854.775807',
    'weekly_out': '9223372036854.775807',
    'monthly_out': '9223372036854.775807',
    'annual_out': '9223372036854.775807'
  },
  'statistics': {
    'daily_outcome': '0.000000',
    'weekly_outcome': '0.000000',
    'monthly_outcome': '0.000000',
    'annual_outcome': '0.000000'
  },
  'policies': {
    'account_policies_type_i': 0,
    'account_policies_types': null
  },
  'external_system_accounts': [
    {
      'type': {
        'name': 'bitcoin',
        'value': 1
      },
      'data': 'n2qdEUgcvoWCxuvRVRBmEvbTHmVEhLSgLx'
    },
    {
      'type': {
        'name': 'ethereum',
        'value': 2
      },
      'data': '0x5b2616CE0f783Cd851Bc882433927dF7735BdFE7'
    }
  ]
}

export const balancesResponse = [
  {
    'balance_id': 'BDH6RGU2WHRDXAIDK5ALIW7NOTBOI6PU63Q7ZXYD6R5A6RPA5ELDSIHT',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'ETH',
    'balance': '84.009021',
    'locked': '0.681095',
    'require_review': false,
    'asset_details': {
      'code': 'ETH',
      'owner': 'GD7AHJHCDSQI6LVMEJEE2FTNCA2LJQZ4R64GUI3PWANSVEO4GEOWB636',
      'available_for_issuance': '9994446.663003',
      'preissued_asset_signer': 'GD7AHJHCDSQI6LVMEJEE2FTNCA2LJQZ4R64GUI3PWANSVEO4GEOWB636',
      'max_issuance_amount': '100000000.000000',
      'issued': '5531.116997',
      'pending_issuance': '0.000000',
      'policy': 26,
      'policies': [
        {
          'name': 'AssetPolicyBaseAsset',
          'value': 2
        },
        {
          'name': 'AssetPolicyWithdrawable',
          'value': 8
        },
        {
          'name': 'AssetPolicyTwoStepWithdrawal',
          'value': 16
        }
      ],
      'details': {
        'logo': {
          'key': '',
          'type': 'logo_type',
          'url': 'logo_url'
        },
        'name': 'ETH name',
        'terms': {
          'key': '',
          'name': '',
          'type': ''
        }
      }
    },
    'converted_balance': '72537.589182',
    'converted_locked': '588.091477'
  },
  {
    'balance_id': 'BDSCHZZB6ZBG2YIXBSD2PNFRIB7SQTB5X5X3N5ZY5RAJQBLCJ7FPG2UV',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'BTC',
    'balance': '96.258068',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'BTC',
      'owner': 'GD7AHJHCDSQI6LVMEJEE2FTNCA2LJQZ4R64GUI3PWANSVEO4GEOWB636',
      'available_for_issuance': '9995066.402001',
      'preissued_asset_signer': 'GD7AHJHCDSQI6LVMEJEE2FTNCA2LJQZ4R64GUI3PWANSVEO4GEOWB636',
      'max_issuance_amount': '100000000.000000',
      'issued': '4933.597999',
      'pending_issuance': '0.000000',
      'policy': 26,
      'policies': [
        {
          'name': 'AssetPolicyBaseAsset',
          'value': 2
        },
        {
          'name': 'AssetPolicyWithdrawable',
          'value': 8
        },
        {
          'name': 'AssetPolicyTwoStepWithdrawal',
          'value': 16
        }
      ],
      'details': {
        'logo': {
          'key': '',
          'type': 'logo_type',
          'url': 'logo_url'
        },
        'name': 'BTC name',
        'terms': {
          'key': '',
          'name': '',
          'type': ''
        }
      }
    },
    'converted_balance': '1027960.122366',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BCJEWTZGGKV4JSIM65ZU5TIXYVYY5JVZL3EI33FCLYZLJNSAE6FLWLG2',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'SWM2',
    'balance': '0.163281',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'SWM2',
      'owner': 'GB5BPLD4FUURS3UBX46FO6PFMYWLGU2WZL2UQVYEVDM3VFVAIEBGM5K2',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GB5BPLD4FUURS3UBX46FO6PFMYWLGU2WZL2UQVYEVDM3VFVAIEBGM5K2',
      'max_issuance_amount': '561.025727',
      'issued': '561.025727',
      'pending_issuance': '0.000000',
      'policy': 0,
      'policies': null,
      'details': {
        'logo': {
          'key': 'dpurgh4ineiubjhcost7fvobrkwq4ijcaisdkwzy4nl3o7jbx3odmmty',
          'type': 'image/png'
        },
        'name': 'SWM2',
        'terms': {
          'key': 'dpurehuineiubjhcost7fvkzskqumgybxejy2t7z66kjharlcgiffnr3',
          'name': 'Screens.png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '7',
          'id': '7',
          'owner_id': 'GB5BPLD4FUURS3UBX46FO6PFMYWLGU2WZL2UQVYEVDM3VFVAIEBGM5K2',
          'base_asset': 'SWM2',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-21T10:00:00Z',
          'end_time': '2018-02-27T10:00:00Z',
          'soft_cap': '1111.000000',
          'hard_cap': '2222.000000',
          'details': {
            'description': '6645JZOLYCKKEAHIRO6OZYDEHJA6DXM2JVCJDUJSSNYNIWIOQYDQ',
            'logo': {
              'key': 'dpurah4ineiubjhcost7fvmb2fkecahejijcb6rqgnabay2m4ykzqkbp',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4ineiubjhcost7fvmb2fkecahejijcb6rqgnabay2m4ykzqkbp'
            },
            'name': 'Name',
            'short_description': 'SWM test sale',
            'youtube_video_id': 'sW17ETznjl8'
          },
          'state': {
            'name': 'closed',
            'value': 2
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '1.000000',
                'quote_balance_id': 'BBDZCUCLLYUHVY23TG4RCI3BXPNIU4DMZQOMFNAZRNW3PNH3TOVRVOLW',
                'current_cap': '0.193858'
              },
              {
                'asset': 'ETH',
                'price': '1.000000',
                'quote_balance_id': 'BCCRDIYSMSNCDKPO6KAJYKFSDVZXVD7OZTPW7S4SDZBAN4BG47S6ZKTM',
                'current_cap': '0.000000'
              },
              {
                'asset': 'SWM',
                'price': '1.000000',
                'quote_balance_id': 'BB5OHOJ3C3E32GDMEYTFY72NBWCZIAJ5ZVUUCMQ54VSJLDP4HEHO7R3H',
                'current_cap': '560.831869'
              }
            ]
          },
          'base_hard_cap': '1111111.000000',
          'base_current_cap': '561.025727',
          'current_cap': '2447.773142',
          'sale_type': {
            'name': 'basic_sale',
            'value': 1
          }
        }
      ]
    },
    'converted_balance': '0.163281',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BACSFUM3UI5QETD47II7HRZ3WIIYCDQEJT3L5SQ4ETWIXKLAIY27PWC7',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'SWM',
    'balance': '174.189583',
    'locked': '2.349585',
    'require_review': false,
    'asset_details': {
      'code': 'SWM',
      'owner': 'GD7AHJHCDSQI6LVMEJEE2FTNCA2LJQZ4R64GUI3PWANSVEO4GEOWB636',
      'available_for_issuance': '5678.000000',
      'preissued_asset_signer': 'GB75ZETBZEAO7W3SMVK2JLBNILUPUKUKKKHVWFM2ARPKG5ZSZBOMATQK',
      'max_issuance_amount': '100000000.000000',
      'issued': '4288.742529',
      'pending_issuance': '0.000000',
      'policy': 90,
      'policies': [
        {
          'name': 'AssetPolicyBaseAsset',
          'value': 2
        },
        {
          'name': 'AssetPolicyWithdrawable',
          'value': 8
        },
        {
          'name': 'AssetPolicyTwoStepWithdrawal',
          'value': 16
        },
        {
          'name': 'AssetPolicyIssuanceManualReviewRequired',
          'value': 64
        }
      ],
      'details': {
        'logo': {
          'key': '',
          'type': ''
        },
        'name': '',
        'terms': {
          'key': '',
          'name': '',
          'type': ''
        }
      }
    },
    'converted_balance': '117.255369',
    'converted_locked': '1.581618'
  },
  {
    'balance_id': 'BBCLRXJ4NCZSZGSVGKVEJT6FZDXYBYTSW5PRKVAVLORIV6TJB3MSQ7EX',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'SWM3',
    'balance': '36991.183131',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'SWM3',
      'owner': 'GC3KLKPJ7K5RFKORGDARBO2R4ROUGVTD65D76X45GDRGY77NP5FRTEZH',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GC3KLKPJ7K5RFKORGDARBO2R4ROUGVTD65D76X45GDRGY77NP5FRTEZH',
      'max_issuance_amount': '99670.365827',
      'issued': '99670.365827',
      'pending_issuance': '0.000000',
      'policy': 0,
      'policies': null,
      'details': {
        'logo': {
          'key': 'dpurgh4inelebjhcost7fvindq5ragmvzrq5mnfaaidslf2n4sxlilir',
          'type': 'image/png'
        },
        'name': 'SWM3',
        'terms': {
          'key': 'dpurehuinelebjhcost7fvia3mxbpycj2wnedgi2i7ep44b33zfjzq56',
          'name': 'swarmtoken.png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '8',
          'id': '8',
          'owner_id': 'GC3KLKPJ7K5RFKORGDARBO2R4ROUGVTD65D76X45GDRGY77NP5FRTEZH',
          'base_asset': 'SWM3',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-24T10:00:00Z',
          'end_time': '2018-03-07T10:00:00Z',
          'soft_cap': '50000.000000',
          'hard_cap': '60000.000000',
          'details': {
            'description': 'NAG2WXHSMGEC4TGERFUI7FKE4I3AVDWFXY6RWKGVJVPNOJE2CASA',
            'logo': {
              'key': 'dpurah4inelebjhcost7fvl77gg4zlb57hqedul4fz4c6fb6rahswdl6',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4inelebjhcost7fvl77gg4zlb57hqedul4fz4c6fb6rahswdl6'
            },
            'name': 'Invest SWM here',
            'short_description': 'You can invest your SWMs here!',
            'youtube_video_id': 'sW17ETznjl8'
          },
          'state': {
            'name': 'closed',
            'value': 2
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '0.000062',
                'quote_balance_id': 'BBCJ7PKHO3QDR7LN3J6W3BQ6DY6O7TUIOTKEPYQLOXIQEAP3Z6OF4QN7',
                'current_cap': '4.294704'
              },
              {
                'asset': 'ETH',
                'price': '0.000725',
                'quote_balance_id': 'BD3R62AHVCHRIBQKQRO4NNBMKRTNCXAB66SHASFI6M7PEVB5SZCAA345',
                'current_cap': '21.465173'
              },
              {
                'asset': 'SWM',
                'price': '1.052628',
                'quote_balance_id': 'BDPQVVOI7OKKYS5KTOLLJ7FCQ7HUBCFZPKC6QZW6F2QAFPFGR4VJZ2QL',
                'current_cap': '835.588000'
              }
            ]
          },
          'base_hard_cap': '100000.000000',
          'base_current_cap': '0.000000',
          'current_cap': '64960.623920',
          'sale_type': {
            'name': 'crowd_funding',
            'value': 2
          }
        }
      ]
    },
    'converted_balance': '36991.183130',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BBSW2CF2YPP2P5B3477Q7UEBTFTB6MUEJM2RIS57MPZZUH6ZTF3G55FI',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'SWM4',
    'balance': '1.241503',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'SWM4',
      'owner': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'max_issuance_amount': '1.248983',
      'issued': '1.248983',
      'pending_issuance': '0.000000',
      'policy': 0,
      'policies': null,
      'details': {
        'logo': {
          'key': 'dpurgh4infpebjhcost7fvmij2wqsc5emyb4z7m4o5jyzuophiql3n72',
          'type': 'image/png'
        },
        'name': 'SWM3 Rmk',
        'terms': {
          'key': 'dpurehuinfpebjhcost7fvifmjrio4gentie6pihtecm7apkalur62ff',
          'name': 'swarmtoken.png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '9',
          'id': '9',
          'owner_id': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
          'base_asset': 'SWM4',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-23T10:00:00Z',
          'end_time': '2018-02-28T10:00:00Z',
          'soft_cap': '10000.000000',
          'hard_cap': '12000.000000',
          'details': {
            'description': 'QO467VLENFWLSCAQT74PYTR2G4NIFNFKEV2BWJNVJECNCNEQ26YA',
            'logo': {
              'key': 'dpurah4infpebjhcost7fvkushnk36zlfzch2q2b3bf5gxfntgzvcr6r',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4infpebjhcost7fvkushnk36zlfzch2q2b3bf5gxfntgzvcr6r'
            },
            'name': 'EWQEQWEWQE',
            'short_description': 'WEQWE',
            'youtube_video_id': 'sW17ETznjl8'
          },
          'state': {
            'name': 'closed',
            'value': 2
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '1.000000',
                'quote_balance_id': 'BBU5CSWOEKKMCPXMSV6X3PMJ2FHVHPQF56T4CLTMQP7O36DBFNOUC2CW',
                'current_cap': '1.248983'
              },
              {
                'asset': 'ETH',
                'price': '1.000000',
                'quote_balance_id': 'BBJ5WHSYJH6QVVLKZKKC5IH2DQ5NQB2PJ3AINJDMXGRKNZGWUNLK7JTY',
                'current_cap': '0.000000'
              },
              {
                'asset': 'SWM',
                'price': '1.000000',
                'quote_balance_id': 'BD572YPMGIIUHQG4SMUSXOHEHQIYTCBO3E6TEIP5FAD57QXYT7MBEF2B',
                'current_cap': '0.000000'
              }
            ]
          },
          'base_hard_cap': '100000.000000',
          'base_current_cap': '1.248983',
          'current_cap': '13338.151743',
          'sale_type': {
            'name': 'basic_sale',
            'value': 1
          }
        }
      ]
    },
    'converted_balance': '1.241503',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BASKOU2DNHAYK46XRR77RJYZ2VZNXXBYDV3DKKQ72TTBSEBRYBZ2RHJ2',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'SPF',
    'balance': '0.000000',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'SPF',
      'owner': 'GDCQWXXHXEQGAOQZQHGNHGMQ33VDJ3RQWOTUBSHX6JFCJ2Z6BOGGA2TS',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GDCQWXXHXEQGAOQZQHGNHGMQ33VDJ3RQWOTUBSHX6JFCJ2Z6BOGGA2TS',
      'max_issuance_amount': '1000000.000000',
      'issued': '0.000000',
      'pending_issuance': '1000000.000000',
      'policy': 32,
      'policies': [
        {
          'name': 'AssetPolicyRequiresKyc',
          'value': 32
        }
      ],
      'details': {
        'logo': {
          'key': 'dpurgh4infgubjhcost7fvklhrmx643j66srda7y5mazzkrnaxndjxc2',
          'type': 'image/png'
        },
        'name': 'S\u0026P 500 Index',
        'terms': {
          'key': 'dpurehuinfgubjhcost7fvmrdxzurbppzj4btpj34anbl6hgajs6tftw',
          'name': 'S_and_P_500_chart_1950_to_2016_with_averages.png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '6',
          'id': '6',
          'owner_id': 'GDCQWXXHXEQGAOQZQHGNHGMQ33VDJ3RQWOTUBSHX6JFCJ2Z6BOGGA2TS',
          'base_asset': 'SPF',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-19T17:00:00Z',
          'end_time': '2018-03-19T16:00:00Z',
          'soft_cap': '10000000.000000',
          'hard_cap': '10000000.000000',
          'details': {
            'description': 'BBEUT45A23KSYJQ6WHFXRPCQKS7Y3L4DR6M5AEYUXVNFMFVBJMCA',
            'logo': {
              'key': 'dpurah4infgubjhcost7fvkatklbxgj7ntfylsz5mtexxpkfob4rvioq',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4infgubjhcost7fvkatklbxgj7ntfylsz5mtexxpkfob4rvioq'
            },
            'name': 'Christopher King',
            'short_description': 'Invests directly into S\u0026P 500 index. ',
            'youtube_video_id': ''
          },
          'state': {
            'name': 'open',
            'value': 1
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '11000.000000',
                'quote_balance_id': 'BBWZFTZ767W5GRDCM3PL7E4W5VFQYOWC2MVKC6IUFDWCC6Z2MUXQ5R22',
                'current_cap': '0.935000'
              },
              {
                'asset': 'ETH',
                'price': '950.000000',
                'quote_balance_id': 'BARLNXMWH7IXUG4UONNG5A6IEZAE6W6X5W5L7P7DIVW4YLWDCYOXNFKO',
                'current_cap': '0.686850'
              }
            ]
          },
          'base_hard_cap': '1000000.000000',
          'base_current_cap': '0.000808',
          'current_cap': '10578.121982',
          'sale_type': {
            'name': 'basic_sale',
            'value': 1
          }
        }
      ]
    },
    'converted_balance': '0.000000',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BA7FHF5GMQXIQINWMBVC3RD6INQ26SGUH4B2ZSKIDRDBRJ7RAJ5PJYLE',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'Test',
    'balance': '148.622709',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'Test',
      'owner': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'max_issuance_amount': '149.999934',
      'issued': '149.999934',
      'pending_issuance': '0.000000',
      'policy': 0,
      'policies': null,
      'details': {
        'logo': {
          'key': 'dpurgh4infpebjhcost7fvlnskyejr7fg3nohtxvs7luetgobnz7h2ai',
          'type': 'image/png'
        },
        'name': 'Test',
        'terms': {
          'key': 'dpurehuinfpebjhcost7fvinty7dqmplc3aaboupellafecdkj7tze6i',
          'name': 'SWM_LOGO (2).png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '11',
          'id': '11',
          'owner_id': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
          'base_asset': 'Test',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-22T10:00:00Z',
          'end_time': '2018-02-28T10:00:00Z',
          'soft_cap': '10000.000000',
          'hard_cap': '20000.000000',
          'details': {
            'description': 'YJIKWOFGLGEQCZAJIGD2XPLFTSW5ULZAJ6AGTPLO6X66EW5TYSPA',
            'logo': {
              'key': 'dpurah4infpebjhcost7fvkcad4c5muje7bo5m6kwjl7siq7vdl3qzja',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4infpebjhcost7fvkcad4c5muje7bo5m6kwjl7siq7vdl3qzja'
            },
            'name': 'Name',
            'short_description': 'eqeqw',
            'youtube_video_id': 'sW17ETznjl8'
          },
          'state': {
            'name': 'closed',
            'value': 2
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '0.013968',
                'quote_balance_id': 'BBU5CSWOEKKMCPXMSV6X3PMJ2FHVHPQF56T4CLTMQP7O36DBFNOUC2CW',
                'current_cap': '0.011730'
              },
              {
                'asset': 'ETH',
                'price': '0.162520',
                'quote_balance_id': 'BBJ5WHSYJH6QVVLKZKKC5IH2DQ5NQB2PJ3AINJDMXGRKNZGWUNLK7JTY',
                'current_cap': '24.017255'
              },
              {
                'asset': 'SWM',
                'price': '236.458444',
                'quote_balance_id': 'BD572YPMGIIUHQG4SMUSXOHEHQIYTCBO3E6TEIP5FAD57QXYT7MBEF2B',
                'current_cap': '326.277751'
              }
            ]
          },
          'base_hard_cap': '150.000000',
          'base_current_cap': '0.000000',
          'current_cap': '21082.599177',
          'sale_type': {
            'name': 'crowd_funding',
            'value': 2
          }
        }
      ]
    },
    'converted_balance': '148.622708',
    'converted_locked': '0.000000'
  },
  {
    'balance_id': 'BAWFHIZBBWVZ5TUTCYVLS6VELMHRE6TKXYVAULWSLCD6MWYAPRDPH2XN',
    'account_id': 'GCJ22MP7JR6HDQ2KANPVKQDN7JVEFCEERLSXVXATRKG6OZU7LB2GHCVN',
    'asset': 'heyhey',
    'balance': '0.000000',
    'locked': '0.000000',
    'require_review': false,
    'asset_details': {
      'code': 'heyhey',
      'owner': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'available_for_issuance': '0.000000',
      'preissued_asset_signer': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
      'max_issuance_amount': '1000000000.000000',
      'issued': '0.000000',
      'pending_issuance': '1000000000.000000',
      'policy': 0,
      'policies': null,
      'details': {
        'logo': {
          'key': 'dpurgh4infpebjhcost7fvl53kh4cia4rv64dclsi4znx3yydushyf6v',
          'type': 'image/png'
        },
        'name': 'hey',
        'terms': {
          'key': 'dpurehuinfpebjhcost7fvllhkr7wncwg4bfbncsrg5b56wpdtze3zor',
          'name': 'swarmtoken.png',
          'type': 'image/png'
        }
      },
      'sales': [
        {
          'paging_token': '10',
          'id': '10',
          'owner_id': 'GBUY24GOTSRWOIW5IZAWXCR3BFFQIGO34C4SVQ6CEPZNC67DUKFWVVOJ',
          'base_asset': 'heyhey',
          'default_quote_asset': 'SUN',
          'start_time': '2018-02-20T10:00:00Z',
          'end_time': '2018-03-20T10:00:00Z',
          'soft_cap': '1000000000.000000',
          'hard_cap': '2000000000.000000',
          'details': {
            'description': 'QO467VLENFWLSCAQT74PYTR2G4NIFNFKEV2BWJNVJECNCNEQ26YA',
            'logo': {
              'key': 'dpurah4infpebjhcost7fvniwuyaam7ki4ou3e5u2ly7o2yqelwegy63',
              'type': 'image/png',
              'url': 'https://staging.storage.sun.swarm.fund/api/dpurah4infpebjhcost7fvniwuyaam7ki4ou3e5u2ly7o2yqelwegy63'
            },
            'name': '1qwqe',
            'short_description': 'eqeq',
            'youtube_video_id': 'sW17ETznjl8'
          },
          'state': {
            'name': 'open',
            'value': 1
          },
          'statistics': {
            'investors': 0
          },
          'quote_assets': {
            'quote_assets': [
              {
                'asset': 'BTC',
                'price': '1.000000',
                'quote_balance_id': 'BBU5CSWOEKKMCPXMSV6X3PMJ2FHVHPQF56T4CLTMQP7O36DBFNOUC2CW',
                'current_cap': '3.033394'
              },
              {
                'asset': 'ETH',
                'price': '1.000000',
                'quote_balance_id': 'BBJ5WHSYJH6QVVLKZKKC5IH2DQ5NQB2PJ3AINJDMXGRKNZGWUNLK7JTY',
                'current_cap': '5.186720'
              },
              {
                'asset': 'SWM',
                'price': '1.000000',
                'quote_balance_id': 'BD572YPMGIIUHQG4SMUSXOHEHQIYTCBO3E6TEIP5FAD57QXYT7MBEF2B',
                'current_cap': '14.300869'
              }
            ]
          },
          'base_hard_cap': '1000000000.000000',
          'base_current_cap': '0.000000',
          'current_cap': '36882.351523',
          'sale_type': {
            'name': 'crowd_funding',
            'value': 2
          }
        }
      ]
    },
    'converted_balance': '0.000000',
    'converted_locked': '0.000000'
  }
]
