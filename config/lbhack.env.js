module.exports = {
  NODE_ENV: '"default"',
  HORIZON_SERVER: '"http://lbhack.tokend.io/_/api"',
  FILE_STORAGE: '"http://storage.lbhack.tokend.io/api"',
  NETWORK_PASSPHRASE: '"LB Hack!"'
}
